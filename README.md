mt19937ar-MersenneTwister-TypeScript
====================================

Random Number Generator.  
I port Mersenne Twister (mt19937ar.c, Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura, All rights reserved.) to TypeScript.  

about Mersenne Twister     
site: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html   
site: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/emt19937ar.html




In Japanese.
---------------------
疑似乱数生成機です。  
Mersenne Twister (mt19937ar.c, Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,  All rights reserved.) の TypeScriptへの移植版です。  
※ほとんどコピペなので私のオリジナリティは皆無ですが一応BSDライセンスに設定しました  

オリジナルの詳細は下記サイトをご覧ください。  
site: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/mt.html   
site: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/mt19937ar.html




開発環境  
---------------------
OS: Windows7 SP1 Starter  
ブラウザ: Vivaldi 3.5.2115.81 (Stable 32-bit) (JavaScript V8 8.7.220.29)  
TypeScript: 3.8.3 (node.js v8.11.2, npm 6.14.4) (Target: ES2020)  
